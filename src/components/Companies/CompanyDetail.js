import LayoutDetail from "../Layuot/LayoutDetail";
import { faAddressCard } from '@fortawesome/free-solid-svg-icons'
import { Button, Col, Form, Nav, Row, Tab, Tabs } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { createCompaniesService, getCompaniesByIdService, updateCompaniesService } from "../API/Companies/service";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { useNavigate } from "react-router";

const CompanyDetail = () => {
    let params = useParams();
    let callBackURL = useNavigate();

    const [companyInput, setCompanyInput] = useState({
        enteredNumber: '',
        companyName: '',
        enteredPhone: '',
        description: ''
    });

    const [requrie, setRequrie] = useState({
        no: false,
        name: false,
    });

    var textTitles = [
        { name_text: "No", detailTitle: companyInput.enteredNumber ? companyInput.enteredNumber : '' },
        { name_text: "Company Name", detailTitle: companyInput.companyName ? companyInput.companyName : '' },
        { name_text: "Phone Number", detailTitle: companyInput.enteredPhone ? companyInput.enteredPhone : '' }
    ]

    useEffect(() => {
        if (params.ID) {
            getCompaniesByIdService(params.ID)
                .then(res => {
                    setCompanyInput({
                        ...companyInput,
                        enteredNumber: res.data.companyNumber,
                        companyName: res.data.companyName,
                        enteredPhone: res.data.phoneNumber,
                        description: res.data.description,
                    });
                })
                .catch(err => {
                    console.error(err);
                })
        }
    }, []);

    const numberChangeHandler = (event) => {
        setCompanyInput({
            ...companyInput,
            enteredNumber: event.target.value,
        });
        setRequrie({ ...requrie, no: false });
    };

    const nameChangeHandler = (event) => {
        setCompanyInput({
            ...companyInput,
            companyName: event.target.value,
        });
        setRequrie({ ...requrie, name: false });
    }
    const phoneChangeHandler = (event) => {
        setCompanyInput({
            ...companyInput,
            enteredPhone: event.target.value,
        });
    }
    const descriptionChangeHandler = (event) => {
        setCompanyInput({
            ...companyInput,
            description: event.target.value,
        });
    }

    const submitHandler = (event) => {
        event.preventDefault();
        const dataList = {
            user: "61fb51ee7bf47e38c08f67ec",
            companyNumber: companyInput.enteredNumber,
            companyName: companyInput.companyName,
            phoneNumber: companyInput.enteredPhone,
            description: companyInput.description
        };

        if (!dataList.companyNumber) {
            setRequrie({ ...requrie, no: true });
            return false;
        }
        
        if (!dataList.companyName) {
            setRequrie({ ...requrie, name: true });
            return false;
        }

        if (params.ID) {
            updateCompaniesService(params.ID, dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Update success',
                    showConfirmButton: false,
                    timer: 2000
                })
            }).catch(err => {
                console(err);
            });
        } else {
            createCompaniesService(dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 2000
                });
                callBackURL('/companies/list');

            }).catch(err => {
                console.log(err)
            });
        }


        // const data = JSON.parse(localStorage.getItem("companies"));
        // if (data) {
        //     console.log(data);
        //     data.map((item) => {
        //         dataList.push(item, companyInput)
        //     });
        // }
        // if (!data) {
        //     dataList.push(companyInput);
        // }

        // localStorage.setItem("companies", JSON.stringify(dataList));
    };

    const button = <div style={{ display: "flex" }}>
        <Nav.Link onClick={submitHandler} style={{ paddingRight: "0rem" }}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Save</Button></Nav.Link>
        <Nav.Link style={{ paddingLeft: "0.5rem" }} href={"/companies/list"}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Cancel</Button></Nav.Link>
    </div>;

    return (
        <>
            <LayoutDetail icon={faAddressCard} title="Companies" textTitle={companyInput.companyName ? companyInput.companyName : "New Companies"} button={button} textTitles={textTitles}></LayoutDetail>
            <Row className='w-100' style={{ height: '82vh' }}>
                <Col sm={8} style={{ borderRight: '0.5rem solid darkgray' }}>
                    <Tabs defaultActiveKey="detail" id="uncontrolled-tab-example" className="ms-2 mt-2">
                        <Tab eventKey="detail" title="Detail">
                            <Form className="ms-2 mt-2">
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>No<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="number" placeholder="" value={companyInput.enteredNumber} onChange={numberChangeHandler} />
                                    {
                                        requrie.no ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>No requrie</span> : ''
                                    }
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Company Name<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="text" placeholder="" value={companyInput.companyName} onChange={nameChangeHandler} />
                                    {
                                        requrie.name ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Company name requrie</span> : ''
                                    }
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Phone Number</Form.Label>
                                    <Form.Control type="number" placeholder="" value={companyInput.enteredPhone} onChange={phoneChangeHandler} />
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlTextarea1">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control as="textarea" type="text" rows={3} value={companyInput.description} onChange={descriptionChangeHandler} />
                                </Form.Group>
                            </Form>
                        </Tab>
                    </Tabs>
                </Col>
                <Col sm={4}>
                    <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="ms-2 mt-2">
                        <Tab eventKey="profile" title="Profile">
                            <label className="ms-2 mt-2">Tab 2</label>
                        </Tab>
                    </Tabs>
                </Col>
            </Row>
        </>
    );
}
export default CompanyDetail;