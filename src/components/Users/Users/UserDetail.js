import LayoutDetail from "../../Layuot/LayoutDetail";
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { Button, Col, Form, Nav, Row, Tab, Tabs } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router";
import { createUserService, getUserByIdService, updateUserService } from "../../API/Users/service";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";

const UserDetail = () => {
    let params = useParams();
    let callBackURL = useNavigate();
    const [userInput, setUserInput] = useState({
        enteredNumber: '',
        userName: '',
        userEmail: '',
        userPass: '',
        enteredPhone: '',
        description: ''
    });
    const [requrie, setRequrie] = useState({
        no: false,
        name: false,
        email: false,
        password: false
    });

    const textTitles = [
        { name_text: "No", detailTitle: '' },
        { name_text: "User Name", detailTitle: userInput.userName ? userInput.userName : '' },
        { name_text: "Phone Number", detailTitle: '' },
        { name_text: "Description", detailTitle: '' }
    ];

    useEffect(() => {
        if (params.ID) {
            getUserByIdService(params.ID)
                .then(res => {
                    setUserInput({
                        ...userInput,
                        userName: res.data.username,
                        userEmail: res.data.email
                    });
                })
                .catch(err => {
                    console.error(err);
                })
        }
    }, []);

    const nameChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            userName: event.target.value,
        });
        setRequrie({ ...requrie, name: false });
    };

    const emailChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            userEmail: event.target.value,
        });
        setRequrie({ ...requrie, email: false })
    };

    const passWordChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            userPass: event.target.value,
        });
        setRequrie({ ...requrie, password: false })
    };

    const submitHandler = (event) => {
        event.preventDefault();
        const dataList = {
            username: userInput.userName,
            email: userInput.userEmail,
            password: userInput.userPass,
        };

        if (!dataList.username) {
            setRequrie({ ...requrie, name: true });
            return false;
        }
        if (!dataList.email) {
            setRequrie({ ...requrie, email: true });
            return false;
        }

        if (params.ID) {
            updateUserService(params.ID, dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Update success',
                    showConfirmButton: false,
                    timer: 2000
                })
            }).catch(err => {
                console(err);
            });
        } else {

            if (!dataList.password) {
                setRequrie({ ...requrie, password: true });
                return false;
            }

            createUserService(dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 2000
                });
                callBackURL('/user/lists');

            }).catch(err => {
                console.log(err)
            });
        }
    };

    const button = <div style={{ display: "flex" }}>
        <Nav.Link onClick={submitHandler} style={{ paddingRight: "0rem" }}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Save</Button></Nav.Link>
        <Nav.Link style={{ paddingLeft: "0.5rem" }} href={"/user/lists"}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Cancel</Button></Nav.Link>
    </div>;

    return (
        <>
            <LayoutDetail icon={faUser} title="User" textTitle={userInput.userName ? userInput.userName : "New User"} button={button} textTitles={textTitles}></LayoutDetail>
            <Row className='w-100' style={{ height: '82vh' }}>
                <Col sm={8} style={{ borderRight: '0.5rem solid darkgray' }}>
                    <Tabs defaultActiveKey="detail" id="uncontrolled-tab-example" className="ms-2 mt-2">
                        <Tab eventKey="detail" title="Detail">
                            <Form className="ms-2 mt-2">
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>No<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="number" placeholder="" />
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>User Name<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="text" placeholder="" value={userInput.userName} onChange={nameChangeHandler} />
                                    {
                                        requrie.name ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Name requrie</span> : ''
                                    }
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Email<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="email" placeholder="Email" value={userInput.userEmail} onChange={emailChangeHandler} />
                                    {
                                        requrie.email ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Email requrie</span> : ''
                                    }
                                </Form.Group>
                                {
                                    params.ID
                                        ? null
                                        : <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Password<span className="text-danger">*</span></Form.Label>
                                            <Form.Control type="password" placeholder="Password" value={userInput.userPass} onChange={passWordChangeHandler} />
                                            {
                                                requrie.password ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Password requrie</span> : ''
                                            }
                                        </Form.Group>
                                }

                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Phone Number</Form.Label>
                                    <Form.Control type="number" placeholder="" />
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlTextarea1">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control as="textarea" rows={3} />
                                </Form.Group>
                            </Form>
                        </Tab>
                    </Tabs>
                </Col>
                <Col sm={4}>
                    <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="ms-2 mt-2">
                        <Tab eventKey="profile" title="Profile">
                            <label className="ms-2 mt-2">Tab 2</label>
                        </Tab>
                    </Tabs>
                </Col>
            </Row>
        </>
    );
}
export default UserDetail;