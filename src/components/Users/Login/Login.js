import React, { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router";
import { Button, Card, Form } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css'
import { authService } from "../../API/Auth/service";
import images from '../../../assets/images/iphone-12.jpg.large.jpg'
import Root from "../../Root";


const UserLogin = () => {
    let callBackURL = useNavigate();
    const userRef = useRef();
    const errRef = useRef();
    const [errMsg, setErrMsg] = useState('');
    const [success, setSuccess] = useState(false);

    const [userInput, setUserInput] = useState({
        enteredFullName: '',
        enteredEmail: '',
        enteredPassword: ''
    });

    useEffect(() => {
        setErrMsg('');
    }, [])

    const fullNameChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            enteredFullName: event.target.value,
        });
    };

    const emailChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            enteredEmail: event.target.value,
        });
    };

    const passwordChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            enteredPassword: event.target.value,
        });
    };

    const loginHandler = () => {
        const user = {
            // full_name: userInput.enteredFullName,
            email: userInput.enteredEmail,
            password: userInput.enteredPassword
        }
        // localStorage.setItem('User', JSON.stringify(user));
        // const userLogin = localStorage.getItem('User')

        authService(user).then(resp => {
            console.log("resp=>", resp);
            setUserInput({
                enteredFullName: '',
                enteredEmail: '',
                enteredPassword: ''
            });
            setSuccess(true);
            callBackURL('/home')
        }).catch(err => {
            if (!err?.response) {
                setErrMsg('No Server Response');
            } else if (err.response?.status === 400) {
                setErrMsg('Missing Username or Password');
            } else if (err.response?.status === 401) {
                setErrMsg('Unauthorized');
            } else {
                setErrMsg('Login Failed');
            }
            errRef.current.focus();
        });
    };

    return (

        <section style={{ height: '100vh', backgroundImage: `url(${images})`, paddingTop: '10%' }}>

            {/* <img src={require('../../../assets/images/iphone 14.jpeg')} style={{width:"auto", height:"50px"}}/> */}

            <Card style={{ width: '30%', margin: 'auto', backgroundColor: '#8f8ff9' }}>
                <Card.Body>
                    <Card.Title style={{ textAlign: 'center' }}>User Login</Card.Title>
                    <p ref={errRef} className={errMsg ? "text-danger" : "offscreen"} aria-live="assertive">{errMsg}</p>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>User Name</Form.Label>
                            <Form.Control type="text" value={userInput.enteredFullName} onChange={fullNameChangeHandler} placeholder="User name" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" value={userInput.enteredEmail} onChange={emailChangeHandler} placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value={userInput.enteredPassword} onChange={passwordChangeHandler} placeholder="Password" />
                        </Form.Group>
                    </Form>

                    <div className="d-grid gap-2">
                        <Button variant="primary" onClick={loginHandler}> Sign In </Button>
                    </div>
                    <p className="mt-2">
                        Need an Account?
                        <span className="line ms-2">
                            <a href="/register">Sign Up</a>
                        </span>
                    </p>
                </Card.Body>
            </Card>
        </section>
    );
};

export default UserLogin;