import React, { useState } from "react";
import { useNavigate } from "react-router";
import { Button, Card, Form } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { registerService } from "../../API/Auth/service";
import Swal from "sweetalert2";

const Register = () => {
    let callBackURL = useNavigate();
    const [requrie, setRequrie] = useState({
        name: false,
        email: false,
        pw: false
    });
    const [userInput, setUserInput] = useState({
        enteredUserName: '',
        enteredEmail: '',
        enteredPhone: '',
        enteredPassword: ''
    });


    const fullNameChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            enteredUserName: event.target.value,
        });
        setRequrie({ ...requrie, name: false });
    };

    const emailChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            enteredEmail: event.target.value,
        });
        setRequrie({ ...requrie, email: false });
    };

    const phoneChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            enteredPhone: event.target.value,
        });

    };

    const passwordChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            enteredPassword: event.target.value,
        });
        setRequrie({ ...requrie, pw: false });
    };

    const RegisterHandler = () => {
        const user = {
            username: userInput.enteredUserName,
            email: userInput.enteredEmail,
            // phone: userInput.enteredPhone,
            password: userInput.enteredPassword
        }

        if (!user.username) {
            setRequrie({ ...requrie, name: true });
            return false;
        }
        if (!user.email) {
            setRequrie({ ...requrie, email: true });
            return false;
        }
        if (!user.password) {
            setRequrie({ ...requrie, pw: true });
            return false;
        }

        registerService(user).then(resp => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Your work has been saved',
                showConfirmButton: false,
                timer: 2000
            });
            setUserInput({
                enteredUserName: '',
                enteredEmail: '',
                enteredPhone: '',
                enteredPassword: ''
            });
            callBackURL('/')
        }).catch(err => {
            console.log(err);
        });
    };
    const callBackHandler = () => {
        callBackURL('/')
    }

    return (
        <Card style={{ width: '40rem', margin: 'auto', marginTop: "10%" }}>
            <Card.Body>
                <Card.Title style={{ textAlign: 'center' }}>Registration User</Card.Title>
                <Card.Text>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>User Name<span className="text-danger">*</span></Form.Label>
                            <Form.Control type="text" value={userInput.enteredUserName} onChange={fullNameChangeHandler} placeholder="User name" />
                            {
                                requrie.name ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>User Name requrie</span> : ''
                            }
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address<span className="text-danger">*</span></Form.Label>
                            <Form.Control type="email" value={userInput.enteredEmail} onChange={emailChangeHandler} placeholder="Enter email" />
                            {
                                requrie.email ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Email requrie</span> : ''
                            }
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Phone Number<span className="text-danger">*</span></Form.Label>
                            <Form.Control type="number" value={userInput.enteredPhone} onChange={phoneChangeHandler} placeholder="Phone Number" />
                            {
                                requrie.phone ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Phone number requrie</span> : ''
                            }
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Password<span className="text-danger">*</span></Form.Label>
                            <Form.Control type="password" value={userInput.enteredPassword} onChange={passwordChangeHandler} placeholder="Password" />
                            {
                                requrie.pw ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Password requrie</span> : ''
                            }
                        </Form.Group>
                    </Form>
                </Card.Text>
                <div style={{float: "right"}}>
                    <Button variant="primary" type="button" onClick={RegisterHandler}>Submit</Button>
                    <Button className="ms-2" variant="secondary" type="button" onClick={callBackHandler}>Callback</Button>
                </div>
            </Card.Body>
        </Card>
    );
};

export default Register;