import LayoutDetail from "../Layuot/LayoutDetail";
import { faCartShopping } from '@fortawesome/free-solid-svg-icons'
import { Button, Col, Form, Nav, Row, Tab, Tabs } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router";
import { useEffect, useState } from "react";
import { createProductService, getProductByIdService, updateProductService } from "../API/Products/service";
import Swal from "sweetalert2";
import { getCategoriesService } from "../API/Categories/service";
import { getBrandsService } from "../API/Brands/service";
const ProductDetail = () => {
    let params = useParams();
    let callBackURL = useNavigate();

    const [categores, setCategory] = useState([]);
    const [brands, setBrand] = useState([]);
    const [productInput, setProductInput] = useState({
        enteredNumber: '',
        productName: '',
        productBrand: '',
        productCategory: '',
        productPrice: '',
        description: ''
    });
    const [requrie, setRequrie] = useState({
        no: false,
        name: false,
        price: false
    });

    const textTitles = [
        { name_text: "Number", detailTitle: productInput.enteredNumber ? productInput.enteredNumber : '' },
        { name_text: "Product Name", detailTitle: productInput.productName ? productInput.productName : '' },
        { name_text: "Brand", detailTitle: productInput.productBrand ? productInput.productBrand : '' },
        { name_text: "Category", detailTitle: productInput.productCategory ? productInput.productCategory : '' },
        { name_text: "Description", detailTitle: productInput.description ? productInput.description : '' }
    ];

    useEffect(() => {
        getBrands();
        getCategories();
        if (params.ID) {
            getProductByIdService(params.ID)
                .then(res => {
                    setProductInput({
                        ...productInput,
                        productName: res.data.name,
                        productBrand: res.data.brand._id,
                        productCategory: res.data.categores._id,
                        productPrice: res.data.price,
                        description: res.data.description,
                    });
                })
                .catch(err => {
                    console.error(err);
                })
        }
    }, []);

    const getCategories = () => {
        getCategoriesService().then(res => {
            setCategory(res.data);
        }).catch(err => {
            console.log(err);
        });

    }

    const getBrands = () => {
        getBrandsService().then(res => {
            setBrand(res.data);
        }).catch(err => {
            console.log(err);
        });

    }

    const nameChangeHandler = (event) => {
        setProductInput({
            ...productInput,
            productName: event.target.value,
        });
        setRequrie({ ...requrie, name: false })
    }

    const brandChangeHandler = (event) => {
        setProductInput({
            ...productInput,
            productBrand: event.target.value,
        });
    }
    const categoriesChangeHandler = (event) => {
        setProductInput({
            ...productInput,
            productCategory: event.target.value,
        });
    }

    const priceChangeHandler = (event) => {
        setProductInput({
            ...productInput,
            productPrice: event.target.value,
        });
        setRequrie({ ...requrie, price: false });
    }

    const descriprionChangeHandler = (event) => {
        setProductInput({
            ...productInput,
            description: event.target.value,
        });
    }
    const submitHandler = (event) => {
        event.preventDefault();
        const dataList = {
            name: productInput.productName,
            brand: productInput.productBrand,
            categores: productInput.productCategory,
            price: productInput.productPrice,
            description: productInput.description
        };

        if (!dataList.name) {
            setRequrie({ ...requrie, name: true });
            return false;
        }
        if (!dataList.price) {
            setRequrie({ ...requrie, price: true });
            return false;
        }

        if (params.ID) {
            updateProductService(params.ID, dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Update success',
                    showConfirmButton: false,
                    timer: 2000
                })
            }).catch(err => {
                console(err);
            });
        } else {
            createProductService(dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 2000
                });
                callBackURL('/products/list');

            }).catch(err => {
                console.log(err)
            });
        }
    }


    const button = <div style={{ display: "flex" }}>
        <Nav.Link onClick={submitHandler} style={{ paddingRight: "0rem" }}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Save</Button></Nav.Link>
        <Nav.Link style={{ paddingLeft: "0.5rem" }} href={"/products/list"}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Cancel</Button></Nav.Link>
    </div>;

    return (
        <>
            <LayoutDetail icon={faCartShopping} title="Product" textTitle={productInput.productName ? productInput.productName : "New Product"} button={button} textTitles={textTitles}></LayoutDetail>
            <Row className='w-100' style={{ height: '82vh' }}>
                <Col sm={8} style={{ borderRight: '0.5rem solid darkgray' }}>
                    <Tabs defaultActiveKey="detail" id="uncontrolled-tab-example" className="ms-2 mt-2">
                        <Tab eventKey="detail" title="Detail">
                            <Form className="ms-2 mt-2">
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>No<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="number" placeholder="" />
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Product Name<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="text" placeholder="" value={productInput.productName} onChange={nameChangeHandler} />
                                    {
                                        requrie.name ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Name requrie</span> : ''
                                    }
                                </Form.Group>

                                <Form.Label>Brand<span className="text-danger">*</span></Form.Label>
                                <Form.Select aria-label="Default select example" className="mb-2" value={productInput.productBrand} onChange={brandChangeHandler}>
                                    {/* <option value=""></option> */}
                                    {
                                        brands.map((item, index) => (
                                            <option key={index} value={item._id}>{item.name}</option>
                                        ))
                                    }
                                </Form.Select>

                                <Form.Label>Category<span className="text-danger">*</span></Form.Label>
                                <Form.Select aria-label="Default select example" className="mb-2" value={productInput.productCategory} onChange={categoriesChangeHandler}>
                                    {/* <option value=""></option> */}
                                    {
                                        categores.map((item, index) => (
                                            <option key={index} value={item._id}>{item.name}</option>
                                        ))
                                    }
                                </Form.Select>

                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Price<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="number" placeholder="" value={productInput.productPrice} onChange={priceChangeHandler} />
                                    {
                                        requrie.price ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>price requrie</span> : ''
                                    }
                                </Form.Group>

                                <Form.Group className="mb-2" controlId="exampleForm.ControlTextarea1">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control as="textarea" type="text" rows={3} value={productInput.description} onChange={descriprionChangeHandler} />
                                </Form.Group>
                            </Form>
                        </Tab>
                    </Tabs>
                </Col>
                <Col sm={4}>
                    <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="ms-2 mt-2">
                        <Tab eventKey="profile" title="Photo">
                            <label className="ms-2 mt-2">Tab 2</label>
                        </Tab>
                    </Tabs>
                </Col>
            </Row>
        </>
    );
}
export default ProductDetail;