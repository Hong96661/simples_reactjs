
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Register from './Users/Register/Register';
import UserLogin from './Users/Login/Login';
import Home from './Home/Home';
import Company from './Companies/Company';
import CompanyDetail from './Companies/CompanyDetail';
import UserLists from './Users/Users/UserList';
import UserDetail from './Users/Users/UserDetail';
import Product from './Products/Product';
import ProductDetail from './Products/ProductDetail';
import Brand from './Brands/Brand';
import BrandDetail from './Brands/BrandDetail';
import Categories from './Categoies/Categories';
import CategoriesDetail from './Categoies/CategoriesDetail';

const Root = () => {
    return (
       
        <>
            <Router>
                <Routes>
                   
                    <Route path="/" element={<UserLogin />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/home" element={<Home />} />
                    {/* route companies */}
                    <Route path="/Companies/list" element={<Company />} />
                    <Route path="/Companies/details" element={<CompanyDetail />} />
                    <Route path="/Companies/details/edit/:ID" element={<CompanyDetail />} />

                    {/* route user */}
                    <Route path="/user/lists" element={<UserLists />} />
                    <Route path="/user/details" element={<UserDetail />} />
                    <Route path="/user/details/edit/:ID" element={<UserDetail />} />
                    
                    {/* route products */}
                    <Route path="/products/list" element={<Product />} />
                    <Route path="/products/details" element={<ProductDetail />} />
                    <Route path="/products/details/edit/:ID" element={<ProductDetail />} />

                    {/* route brands */}
                    <Route path="/brands/list" element={<Brand />} />
                    <Route path="/brands/details" element={<BrandDetail />} />
                    <Route path="/brands/details/edit/:ID" element={<BrandDetail />} />

                    {/* route categories */}
                    <Route path="/categories/list" element={<Categories />} />
                    <Route path="/categories/details" element={<CategoriesDetail />} />
                    <Route path="/categories/details/edit/:ID" element={<CategoriesDetail />} />
                </Routes>
            </Router>
        </>
    );
}
export default Root;