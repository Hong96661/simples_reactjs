import { Button, Col, FormControl, InputGroup, Row, Table } from "react-bootstrap";
import Layout from "../Layuot/Layout";
import { faShuffle, faMagnifyingGlass, faTrashCan, faPenToSquare } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { deleteBrandService, getBrandsService } from "../API/Brands/service";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";

class Brand extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            brands: [],
            loading: true,
            filteredData: []
        };
    };

    componentDidMount() {
        this.requestBrand();
    };

    requestBrand() {
        getBrandsService()
            .then(res => {
                this.setState({
                    brands: res.data, loading: false,
                    filteredData: res.data
                });
            })
            .catch(err => {
                this.setState({ loading: false });
                console.error(err);
            })
    };
    _handleSearchChange = e => {
        const { value } = e.target;
        const lowercasedValue = value.toLowerCase();
        this.setState(prevState => {
            const filteredData = prevState.brands.filter(el =>
                el.name.toLowerCase().includes(lowercasedValue)
            );
            return { filteredData };
        });
    };

    deletedHandler(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                this.setState({ loading: false });
                deleteBrandService(id).then(resp => {
                    Swal.fire({
                        icon: 'success',
                        title: 'deleted item success!',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    this.requestBrand();
                }).catch(err => {
                    console.log(err)
                });
            }
        })
    };

    render() {
        const { filteredData, loading } = this.state;
        return (
            <>
                <Layout title={"Brand"} textTitle={"Brands"} icon={faShuffle} button={true} route={"brands/details"} />
                <div className="mx-3">
                    <Row className="mt-3">
                        <Col sm={8}></Col>
                        <Col sm={4}>
                            <InputGroup className="mb-3">
                                <FormControl
                                    placeholder="Search Name"
                                    aria-label="Search Name"
                                    aria-describedby="basic-addon2"
                                    onChange={this._handleSearchChange}
                                />
                                <Button variant="outline-secondary" id="button-addon2">
                                    <FontAwesomeIcon icon={faMagnifyingGlass} />
                                </Button>
                            </InputGroup>
                        </Col>
                    </Row>

                    <Table responsive="sm">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Brand name</th>
                                <th>description</th>
                                <th style={{ textAlign: 'end' }}>Action</th>
                            </tr>
                        </thead>
                        <tbody style={{ borderTop: "none" }}>
                            {
                                !loading ?
                                    filteredData.length > 0 ?
                                        filteredData.map((item, index) => (
                                            <tr key={index}>
                                                <td>{item._id}</td>
                                                <td>{item.name}</td>
                                                <td>{item.description}</td>
                                                <td style={{ textAlign: 'end' }}>
                                                    <Button variant="outline-danger" size="sm" onClick={e => { this.deletedHandler(item._id) }}><FontAwesomeIcon icon={faTrashCan} /></Button>
                                                    <Link to={`/brands/details/edit/${item._id}`}>
                                                        <Button variant="outline-secondary" size="sm" className="ms-1" >
                                                            <FontAwesomeIcon icon={faPenToSquare} />
                                                        </Button>
                                                    </Link>
                                                </td>
                                            </tr>
                                        )) :
                                        <tr>
                                            <td colSpan={4} style={{ textAlign: 'center', borderBottom: 'none' }}>
                                                No record!
                                            </td>
                                        </tr>
                                    :
                                    <tr>
                                        <td colSpan={4} style={{ textAlign: 'center', borderBottom: 'none' }}>
                                            Loading....
                                        </td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </div>
            </>
        );
    }
}

export default Brand;