import LayoutDetail from "../Layuot/LayoutDetail";
import { faShuffle } from '@fortawesome/free-solid-svg-icons'
import { Button, Col, Form, Nav, Row, Tab, Tabs } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router";
import { useEffect, useState } from "react";
import { createBrandService, getBrandByIdService, updateBrandService } from "../API/Brands/service";
import Swal from "sweetalert2";

const BrandDetail = () => {
    let params = useParams();
    let callBackURL = useNavigate();

    const [brandInput, setBrandInput] = useState({
        brandName: '',
        description: ''
    });
    const [requrie, setRequrie] = useState({
        no: false,
        name: false,
    });

    const textTitles = [
        { name_text: "No", detailTitle: '' },
        { name_text: "Brand Name", detailTitle: brandInput.brandName ? brandInput.brandName : '' },
        { name_text: "Description", detailTitle: brandInput.description ? brandInput.description : '' }
    ];

    useEffect(() => {
        if (params.ID) {
            getBrandByIdService(params.ID)
                .then(res => {
                    setBrandInput({
                        ...brandInput,
                        brandName: res.data.name,
                        description: res.data.description,
                    });
                })
                .catch(err => {
                    console.error(err);
                })
        }
    }, []);

    const nameChangeHandler = (event) => {
        setBrandInput({
            ...brandInput,
            brandName: event.target.value,
        });
        setRequrie({ ...requrie, name: false })
    }

    const descriptionChangeHandler = (event) =>{
        setBrandInput({
            ...brandInput,
            description: event.target.value,
        });
    }

    const submitHandler = (event) => {
        event.preventDefault();
        const dataList = {
            name: brandInput.brandName,
            description: brandInput.description
        };

        if (!dataList.name) {
            setRequrie({ ...requrie, name: true });
            return false;
        }

        if (params.ID) {
            updateBrandService(params.ID, dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Update success',
                    showConfirmButton: false,
                    timer: 2000
                })
            }).catch(err => {
                console(err);
            });
        } else {
            createBrandService(dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 2000
                });
                callBackURL('/brands/list');

            }).catch(err => {
                console.log(err)
            });
        }
    }

    const button = <div style={{ display: "flex" }}>
        <Nav.Link onClick={submitHandler}  style={{ paddingRight: "0rem" }}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Save</Button></Nav.Link>
        <Nav.Link style={{ paddingLeft: "0.5rem" }} href={"/brands/list"}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Cancel</Button></Nav.Link>
    </div>;

    return (
        <>
            <LayoutDetail icon={faShuffle} title="Brand" textTitle={brandInput.brandName ? brandInput.brandName : "New Brand"} button={button} textTitles={textTitles}></LayoutDetail>
            <Row className='w-100' style={{ height: '82vh', margin: 'auto' }}>
                <Col>
                    <Tabs defaultActiveKey="detail" id="uncontrolled-tab-example" className="ms-2 mt-2">
                        <Tab eventKey="detail" title="Detail">
                            <Form className="ms-2 mt-2">
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>No<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="number" placeholder="" />
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Name<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="text" placeholder=""  value={brandInput.brandName} onChange={nameChangeHandler} />
                                    {
                                        requrie.name ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Name requrie</span> : ''
                                    }
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlTextarea1">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control as="textarea" type="text" rows={3} value={brandInput.description} onChange={descriptionChangeHandler}/>
                                </Form.Group>
                            </Form>
                        </Tab>
                    </Tabs>
                </Col>
            </Row>
        </>
    );
}
export default BrandDetail;