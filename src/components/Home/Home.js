import React, { useState } from 'react';
import { Card, Carousel, Col, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'

// import Expeneses from '../Expeneses/Expeneses';
// import NewExpenese from '../NewExpenes/NewExpeneses';
import Layout from '../Layuot/Layout';
import { faHouse } from '@fortawesome/free-solid-svg-icons';

// const DUMMY_EXPENSES = [
//     {
//         id: "001",
//         date: new Date(2022, 2, 8),
//         title: "Toilet  Paper",
//         amount: 294.67
//     },
//     {
//         id: "002",
//         date: new Date(2022, 3, 5),
//         title: "New TV",
//         amount: 400.50
//     },
//     {
//         id: "003",
//         date: new Date(2021, 4, 3),
//         title: "Car Insurance",
//         amount: 200.90
//     },
//     {
//         id: "004",
//         date: new Date(2020, 5, 15),
//         title: "New Desk (Wooden)",
//         amount: 500.90
//     }
// ];

const Home = () => {
    // const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

    // const addExpenseHandler = expense => {
    //     setExpenses((prevExpenses) => {
    //         return [expense, ...prevExpenses]
    //     });
    // }

    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };
    return (
        <>
            <Layout title={"Home"} textTitle={"Home"} icon={faHouse}/>
            {/* <NewExpenese onAddExpense={addExpenseHandler} /> */}
            {/* <Expeneses items={expenses} /> */}
            <Carousel activeIndex={index} onSelect={handleSelect} className="w-100">
                {Array.from({ length: 3 }).map((_, idx) => (
                    <Carousel.Item key={idx}>
                        <img style={{ width: 'inherit', height: '35rem' }}
                            className="d-block"
                            src={require('../../assets/images/iphone 14.jpeg')}
                            alt="First slide"
                        />
                        <Carousel.Caption>
                            <h3>First slide label</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                ))}
            </Carousel>

            <Row xs={1} md={3} className="g-3" style={{ marginTop: '0px' }}>
                {Array.from({ length: 6 }).map((_, idx) => (
                    <Col key={idx}>
                        <Card>
                            <Card.Img variant="top" src={require('../../assets/images/iphone 14.jpeg')} />
                            <Card.Body>
                                <Card.Title>Iphone 14 pro</Card.Title>
                                <Card.Text>
                                    iPhone 14 Pro | iPhone 14 Pro price and specifications, the most important features and defects - Specifications-Pro Visit.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                ))}
            </Row>
        </>
    );
}

export default Home;