export function encryptParams(p) {
  if (p) {
    console.log("HTTP REQUEST: ", p);
  }

  return p;
}

export function formatMoney(amount) {
  if(!amount) {
    return '¥0.00';
  }
  return '¥' + (amount - 0).toFixed(2);
}


