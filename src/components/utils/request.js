import axios from "axios";
import { API_ENDPOINT } from "../Config/api";
import { encryptParams } from "./string";

const instance = axios.create({
    baseURL: API_ENDPOINT,
    transformRequest: [data => JSON.stringify(encryptParams(data))]
});
instance.interceptors.request.use(
    async config => {
        config.headers = {
            "Content-Type": "application/json;charset=UTF-8",
            Accept: "application/json"
        };
        return config;
    },
    err => Promise.reject(err)
);

// instance.interceptors.response.use(
//     response => response,
//     error => {
//         console.log("Error Response: ", error);
//         return Promise.reject(error.response.data);
//     }
// );


export default instance;