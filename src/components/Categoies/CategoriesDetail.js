import LayoutDetail from "../Layuot/LayoutDetail";
import { faShuffle } from '@fortawesome/free-solid-svg-icons'
import { Button, Col, Form, Nav, Row, Tab, Tabs } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router";
import { useEffect, useState } from "react";
import { createCategoriesService, getCategoriesByIdService, updateCategoriesService } from "../API/Categories/service";
import Swal from "sweetalert2";

const CategoriesDetail = () => {
    let params = useParams();
    let callBackURL = useNavigate();

    const [categoriesInput, setCategoriesInput] = useState({
        categoriesName: '',
        description: ''
    });
    const [requrie, setRequrie] = useState({
        no: false,
        name: false,
    });

    const textTitles = [
        { name_text: "No", detailTitle: '' },
        { name_text: "Name", detailTitle: categoriesInput.categoriesName ? categoriesInput.categoriesName : '' },
        { name_text: "Description", detailTitle: categoriesInput.description ? categoriesInput.description : '' }
    ]

    useEffect(() => {
        if (params.ID) {
            getCategoriesByIdService(params.ID)
                .then(res => {
                    setCategoriesInput({
                        ...categoriesInput,
                        categoriesName: res.data.name,
                        description: res.data.description,
                    });
                })
                .catch(err => {
                    console.error(err);
                })
        }
    }, []);

    const nameChangeHandler = (event) => {
        setCategoriesInput({
            ...categoriesInput,
            categoriesName: event.target.value,
        });
        setRequrie({ ...requrie, name: false })
    }

    const descriptionChangeHandler = (event) =>{
        setCategoriesInput({
            ...categoriesInput,
            description: event.target.value,
        });
    }

    const submitHandler = (event) => {
        event.preventDefault();
        const dataList = {
            name: categoriesInput.categoriesName,
            description: categoriesInput.description
        };

        if (!dataList.name) {
            setRequrie({ ...requrie, name: true });
            return false;
        }

        if (params.ID) {
            updateCategoriesService(params.ID, dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Update success',
                    showConfirmButton: false,
                    timer: 2000
                })
            }).catch(err => {
                console(err);
            });
        } else {
            createCategoriesService(dataList).then(resp => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 2000
                });
                callBackURL('/categories/list');

            }).catch(err => {
                console.log(err)
            });
        }
    }


    const button = <div style={{ display: "flex" }}>
        <Nav.Link onClick={submitHandler} style={{ paddingRight: "0rem" }}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Save</Button></Nav.Link>
        <Nav.Link style={{ paddingLeft: "0.5rem" }} href={"/categories/list"}><Button variant="outline-secondary" style={{ borderColor: ' rgba(0,0,0,.1)' }}>Cancel</Button></Nav.Link>
    </div>;

    return (
        <>
            <LayoutDetail icon={faShuffle} title="Category" textTitle={categoriesInput.categoriesName ? categoriesInput.categoriesName : "New Category"} button={button} textTitles={textTitles}></LayoutDetail>
            <Row className='w-100' style={{ height: '82vh', margin: 'auto' }}>
                <Col>
                    <Tabs defaultActiveKey="detail" id="uncontrolled-tab-example" className="ms-2 mt-2">
                        <Tab eventKey="detail" title="Detail">
                            <Form className="ms-2 mt-2">
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>No<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="number" placeholder="" />
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Name<span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="text" placeholder="" value={categoriesInput.categoriesName} onChange={nameChangeHandler} />
                                    {
                                        requrie.name ? <span className="text-danger" style={{ fontSize: '0.8rem' }}>Name requrie</span> : ''
                                    }
                                </Form.Group>
                                <Form.Group className="mb-2" controlId="exampleForm.ControlTextarea1">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control as="textarea" rows={3} type="text" value={categoriesInput.description} onChange={descriptionChangeHandler} />
                                </Form.Group>
                            </Form>
                        </Tab>
                    </Tabs>
                </Col>
            </Row>
        </>
    );
}
export default CategoriesDetail;