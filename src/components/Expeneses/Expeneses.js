import ExpenseItem from './ExpenseItem';
import './Expeneses.css';
import Card from '../UI/Card';
import ExpenesesFilter from './ExpenesesFilter';
import React, { useState } from 'react';

const Expeneses = (props) => {
    const [filterYear, setFilteredYear] = useState('2022');

    const filterChangeHandler = (filterYear) => {
        setFilteredYear(filterYear);
    };
    const filteredExpense = props.items.filter(expense => {
        return expense.date.getFullYear().toString() === filterYear;
    });

    let expenseContent = <p className='expeneses__controller-p'>No expenses found.</p>;
    if (filteredExpense.length > 0) {
        expenseContent = filteredExpense.map((item) => (
            <ExpenseItem
                key={item.id}
                date={item.date}
                title={item.title}
                price={item.amount}
            />
        ));
    }

    return (
        <Card className="expeneses">
            <ExpenesesFilter selected={filterYear} onChangeFilter={filterChangeHandler} />
            {
                expenseContent
            }
        </Card>
    )
}
export default Expeneses;