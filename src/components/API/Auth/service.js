import request from "../../utils/request";

export const authService = param => request.post("auth/login", param);
export const registerService = param => request.post("auth/register", param);