
import request from "../../utils/request";

export const getCompaniesService = () => request.get("auth/companies/");
export const getCompaniesByIdService = (id, param) => request.get("auth/companies/" + id, param);
export const createCompaniesService = param => request.post("auth/companies", param);
export const updateCompaniesService = (id, param) => request.patch("auth/companies/" + id, param);
export const deleteCompaniesService = (id, param) => request.delete("/auth/companies/" + id, param);
// export const API_COMPANY = "/auth/companies";