import request from "../../utils/request";

export const getBrandsService = () => request.get("auth/brands");
export const getBrandByIdService = (id) => request.get("/auth/brands/" + id);
export const createBrandService = param => request.post("auth/brands", param);
export const updateBrandService = (id, param) => request.patch("/auth/brands/" + id, param);
export const deleteBrandService = (id, param) => request.delete("/auth/brands/" + id, param);