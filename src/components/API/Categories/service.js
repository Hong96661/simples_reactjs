
import request from "../../utils/request";

export const getCategoriesService = () => request.get("auth/categores");
export const getCategoriesByIdService = (id) => request.get("/auth/categores/" + id);
export const createCategoriesService = param => request.post("auth/categores", param);
export const updateCategoriesService = (id, param) => request.patch("/auth/categores/" + id, param);
export const deleteCategoriesService = (id, param) => request.delete("/auth/categores/" + id, param);