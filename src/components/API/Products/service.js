
import request from "../../utils/request";

export const getProductsService = () => request.get("auth/products");
export const getProductByIdService = (id) => request.get("/auth/products/" + id);
export const createProductService = param => request.post("auth/products", param);
export const updateProductService = (id, param) => request.patch("/auth/products/" + id, param);
export const deleteProductService = (id, param) => request.delete("/auth/products/" + id, param);