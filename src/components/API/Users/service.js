import request from "../../utils/request";

export const getUsersService = () => request.get("auth/users");
export const getUserByIdService = (id, param) => request.get("auth/users/" + id, param);
export const createUserService = param => request.post("auth/users", param);
export const updateUserService = (id, param) => request.patch("auth/users/" + id, param);
export const deleteUserService = (id) => request.delete("/auth/users/" + id);