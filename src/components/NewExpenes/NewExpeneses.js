import React from "react";
import ExpeneseForm from "./ExpeneseForm";
import './NewExpeneses.css';

const NewExpenese = (props) =>{

    const saveExpenseDataHandler = (enteredExpenseData) =>{
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString()
        };
        props.onAddExpense(expenseData);
    };

    return (
        <div className="new-expense">
            <ExpeneseForm onSaveExpenseData={saveExpenseDataHandler}/>
        </div>
    );
}
export default NewExpenese;