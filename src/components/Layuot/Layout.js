import React from 'react';
import { Button, Col, Container, Nav, Offcanvas, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Navbar from 'react-bootstrap/Navbar'
import 'bootstrap/dist/css/bootstrap.min.css'
import './Layout.css';
const Layout = (props) => {
    const button = <Nav.Link href={"/" + props.route}><Button variant="outline-secondary" style={{borderColor:' rgba(0,0,0,.1)'}}>New</Button></Nav.Link>;
    return (
        <Navbar expand={false} style={{ backgroundColor: "darkgray", paddingTop: '0rem', paddingBottom: '0.4rem' }}>
            <Container fluid style={{ backgroundColor: 'lavender' }}>
                <Nav className="me-auto ms-2">
                    <Row>
                        <Col>
                            <div className='layout__header-icon'>
                                <FontAwesomeIcon icon={props.icon} />
                                <div className='layout__header ms-3'>
                                    <div className='layout__title'>{props.title}</div>
                                    <div className='layout__text__tile'>{props.textTitle}</div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Nav>
                {
                    props.button ? button : ''
                }
                <Navbar.Toggle aria-controls="offcanvasNavbar" />
                <Navbar.Offcanvas
                    id="offcanvasNavbar"
                    aria-labelledby="offcanvasNavbarLabel"
                    placement="end"
                >
                    <Offcanvas.Header closeButton>
                        <Offcanvas.Title id="offcanvasNavbarLabel">Offcanvas</Offcanvas.Title>
                    </Offcanvas.Header>
                    <Offcanvas.Body>
                        <Nav className="justify-content-end flex-grow-1 pe-3">
                            <Nav.Link href="/home">Home</Nav.Link>
                            <Nav.Link href="/companies/list">Companies</Nav.Link>
                            <Nav.Link href="/user/lists">Users</Nav.Link>
                            <Nav.Link href="/products/list">Products</Nav.Link>
                            <Nav.Link href="/brands/list">Brands</Nav.Link>
                            <Nav.Link href="/categories/list">Categories</Nav.Link>
                        </Nav>
                    </Offcanvas.Body>
                </Navbar.Offcanvas>
            </Container>
        </Navbar>
    );
}

export default Layout;