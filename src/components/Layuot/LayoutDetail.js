import React from 'react';
import { Col, Container, Nav, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Navbar from 'react-bootstrap/Navbar'
import 'bootstrap/dist/css/bootstrap.min.css'
import './LayoutDetail.css';
const LayoutDetail = (props) => {
     
    return (
        <Navbar expand={false} style={{ backgroundColor: "darkgray", paddingTop: '0rem', paddingBottom: '0.4rem' }}>
            <Container fluid style={{ backgroundColor: 'lavender' }} className="pe-0">
                <Nav className="me-auto ms-2">
                    <Row>
                        <Col>
                            <div className='layout__header-detail-icon'>
                                <FontAwesomeIcon icon={props.icon} />
                                <div className='layout__header-detail ms-3'>
                                    <div className='layout__title-detail'>{props.title}</div>
                                    <div className='layout__text__tile-detail'>{props.textTitle}</div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Nav>
                {
                    props.button ? props.button : ''
                }
            </Container>
            <Container fluid style={{ backgroundColor: '#e6e6e6' }}>
                <Row className='w-100 p-2'>
                    {
                        props.textTitles.map((text, index) => (
                            <Col key={index}>
                                <label>{text.name_text}</label>
                                <p>{text.detailTitle}</p>
                            </Col>
                        ))
                    }
                </Row>
            </Container>
        </Navbar>
    );
}

export default LayoutDetail;