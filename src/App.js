import React from 'react';
import './App.css';
import Root from './components/Root';
import UserLogin from './components/Users/Login/Login';

const App = () => {

  return (
    <>
      {/* <Layout/> */}
      {/* <Home></Home> */}
      {/* <UserLogin></UserLogin> */}
      <Root />
    </>

  );
}

export default App;
